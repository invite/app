import os
import shutil

base_path = os.path.dirname(__file__)
image_path = os.path.join(base_path, 'images')

batches = [b for b in os.listdir(base_path) if (os.path.isdir(b) and b != 'images')]

os.makedirs('./images', exist_ok=True)

for batch in batches:
    batch_path = os.path.join(base_path, batch, 'raw')

    files = [f for f in os.listdir(batch_path) if f.endswith('.png')]

    for file in files:
        old = os.path.join(batch_path, file)
        new = os.path.join(image_path, (batch + '_' + file))

        shutil.copy(old, new)
