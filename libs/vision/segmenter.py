## general libraries
from pathlib import Path

import numpy as np
import os
import cv2
import csv
import random
import operator
from collections import OrderedDict

import torchvision
from tqdm import tqdm
import time
from itertools import groupby
import json
import warnings

warnings.filterwarnings("ignore")

## detectron2-libraries 
import detectron2
from detectron2.utils.logger import setup_logger

setup_logger()
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import DatasetCatalog, MetadataCatalog, build_detection_test_loader
from detectron2.data.datasets import register_coco_instances
from detectron2.engine import DefaultTrainer
from detectron2.evaluation import COCOEvaluator, inference_on_dataset
from detectron2.modeling import build_model

##custom functions
# from generic_detectron.convert_datasets.utils_dataset_preparation import check_direxcist
## the training id will create a new folder within the basefolder
# in this folder the weights and results will be saved
training_id = "id_1"

basefolder = '/home/ubuntu2004/W-drive-vision/AGROS_Autonomous_Greenhouse/2021-1-21_cucumber_measurements/cucumber_annotated/'
# dataroot = basefolder + 'images/'
# train_name = 'train'
# val_name = 'val'
# test_name = 'test'

weightsfolder = os.path.join(Path(__file__).resolve().parent, 'models')
resultsfolder = os.path.join(Path(__file__).resolve().parent, 'models', 'results')

# register_coco_instances(train_name, {}, basefolder + train_name + '.json', '')
# register_coco_instances(val_name, {}, basefolder + val_name + '.json', '')
# register_coco_instances(test_name, {}, basefolder + test_name + '.json', '')

# train_metadata = MetadataCatalog.get("train")
# val_metadata = MetadataCatalog.get("val")
test_metadata = MetadataCatalog.get("test")


class Segmentron():
    def __init__(self) -> None:
        print('initialising detectron')
        config_file = "COCO-InstanceSegmentation/mask_rcnn_X_101_32x8d_FPN_3x.yaml"
        classes = ['Upright', 'Face-Down Tomato', 'Sideview']

        cfg = get_cfg()
        cfg.merge_from_file(model_zoo.get_config_file(config_file))
        # cfg.DATASETS.TRAIN = ("train",)
        # cfg.DATASETS.TEST = ("val",)

        cfg.NUM_GPUS = 1
        cfg.DATALOADER.NUM_WORKERS = 2
        cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(config_file)
        cfg.MODEL.DEVICE = 'cpu'
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(classes)

        cfg.INPUT.MAX_SIZE_TRAIN = 1280
        cfg.INPUT.MAX_SIZE_TEST = 1280

        cfg.INPUT.MIN_SIZE_TRAIN = 720
        cfg.INPUT.MIN_SIZE_TEST = 720

        cfg.OUTPUT_DIR = weightsfolder
        os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)

        cfg.OUTPUT_DIR = weightsfolder  ##normally
        cfg.MODEL.WEIGHTS = os.path.join(cfg.OUTPUT_DIR, 'segmenter', 'segmentation.pth')

        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.8  # set the testing threshold for this model
        cfg.MODEL.ROI_HEADS.NMS_THRESH_TEST = 0.5

        self.iou_th = cfg.MODEL.ROI_HEADS.NMS_THRESH_TEST

        print('initialising predictor')
        self.predictor = DefaultPredictor(cfg)
        print('detectron network loaded')

    def process_single(self, file_name):
        """Processing a single image, input is str file_name"""

        img = cv2.imread(file_name)
        t1 = time.time()
        outputs = self.predictor(img)

        outputs['instances'] = outputs['instances'][
            torchvision.ops.nms(outputs['instances'].pred_boxes.tensor, outputs['instances'].scores, self.iou_th)]

        # print(outputs['instances'].pred_masks[0,:,:].numpy())
        # print(outputs["instances"])

        print(time.time() - t1)
        # visualizer = Visualizer(img[:, :, ::-1], metadata=test_metadata, scale=0.8)
        # vis = visualizer.draw_instance_predictions(outputs["instances"].to("cpu"))
        # bgr_output = vis.get_image()[:, :, ::-1]

        # return outputs, bgr_output
        return outputs


if __name__ == '__main__':
    classes = ['up', 'down', 'side']
    # image = os.path.join(weightsfolder, 'test', 'rgb_raw.png')
    # image = os.path.join(weightsfolder, 'test', 'rgb_raw_mixed.png')
    # image = os.path.join(weightsfolder, 'test', 'rgb_raw_up.png')
    image = os.path.join(weightsfolder, 'test', 'L515_test1511_tailored_rgb_aligned.png')

    dl = Segmentron()
    outputs, bgr_output = dl.process_single(image)
    # print(outputs)

    # img = cv2.imread(image)
    # gray = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
    # # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #
    # for idx, mask in enumerate(outputs['instances'].pred_masks.numpy()):
    #     orientation = outputs['instances'].pred_classes[idx].numpy()
    #
    #     zeros = np.zeros(gray.shape, dtype='uint8')
    #     img_mask = np.uint8(np.where(mask, 255, 0))
    #     masked_img = cv2.bitwise_and(gray, gray, mask=img_mask)
    #
    #     (y, x) = np.where(img_mask == 255)
    #     (topy, topx) = ((np.min(y) - 12), (np.min(x) - 12))
    #     (bottomy, bottomx) = ((np.max(y) + 12), (np.max(x) + 12))
    #     cropped = img[topy:bottomy + 1, topx:bottomx + 1]
    #     #
    #     # cv2.imshow('img', cropped)
    #     # cv2.waitKey(0)
    #
    #     cv2.imwrite(os.path.join(weightsfolder, 'test',  str(idx + 1) + '.' + classes[orientation] + '.png'), cropped)
    #
    # import matplotlib.pyplot as plt
    #
    # plt.imshow(bgr_output[:, :, ::-1])
    # plt.show()
