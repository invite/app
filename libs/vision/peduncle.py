import os
import time
import torchvision

from detectron2.utils.logger import setup_logger

setup_logger()
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import DatasetCatalog, MetadataCatalog, build_detection_test_loader
from detectron2.data.datasets import register_coco_instances
from detectron2.engine import DefaultTrainer
from detectron2.evaluation import COCOEvaluator, inference_on_dataset
from detectron2.modeling import build_model

from skimage.segmentation import flood_fill


WEIGHTSFOLDER = os.path.join(os.path.dirname(__file__), 'models', 'peduncle')


class PeduncleSegmentron():
    def __init__(self) -> None:
        print('initialising detectron')
        config_file = 'COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_1x.yaml'
        classes = ['Peduncle scar']
        input_size = 160

        cfg = get_cfg()
        cfg.merge_from_file(model_zoo.get_config_file(config_file))
        # cfg.DATASETS.TRAIN = ("train",)
        # cfg.DATASETS.TEST = ("val",)

        cfg.NUM_GPUS = 1
        cfg.DATALOADER.NUM_WORKERS = 2
        cfg.MODEL.WEIGHTS = os.path.join(WEIGHTSFOLDER, 'peduncle.pth')
        cfg.MODEL.DEVICE = 'cpu'
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(classes)

        cfg.INPUT.MAX_SIZE_TRAIN = input_size
        cfg.INPUT.MAX_SIZE_TEST = input_size

        cfg.INPUT.MIN_SIZE_TRAIN = input_size
        cfg.INPUT.MIN_SIZE_TEST = input_size

        cfg.OUTPUT_DIR = WEIGHTSFOLDER
        os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)

        cfg.OUTPUT_DIR = WEIGHTSFOLDER  ##normally
        # cfg.MODEL.WEIGHTS = os.path.join(cfg.OUTPUT_DIR, 'segmenter', 'segmentation.pth')

        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.8  # set the testing threshold for this model
        cfg.MODEL.ROI_HEADS.NMS_THRESH_TEST = 0.5

        self.iou_th = cfg.MODEL.ROI_HEADS.NMS_THRESH_TEST

        print('initialising predictor')
        self.predictor = DefaultPredictor(cfg)
        print('detectron network loaded')

    def process_single(self, img):
        """Processing a single image, input is str file_name"""
        t1 = time.time()
        outputs = self.predictor(img)

        outputs['instances'] = outputs['instances'][
            torchvision.ops.nms(outputs['instances'].pred_boxes.tensor, outputs['instances'].scores, self.iou_th)]

        # print(outputs['instances'].pred_masks[0,:,:].numpy())
        # print(outputs["instances"])

        # print(time.time() - t1)
        # visualizer = Visualizer(img[:, :, ::-1], metadata=test_metadata, scale=0.8)
        # vis = visualizer.draw_instance_predictions(outputs["instances"].to("cpu"))
        # bgr_output = vis.get_image()[:, :, ::-1]

        # return outputs, bgr_output
        m = outputs['instances'].pred_masks.cpu()

        if m.shape[0] != 0:
            return m[0, :, :]
        else:
            return None