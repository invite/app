import os
import time

import cv2

import numpy as np

from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2 import model_zoo

from skimage.segmentation import flood_fill
from matplotlib import pyplot as plt

WEIGHTSFOLDER = os.path.join(os.path.dirname(__file__), 'models', 'segmenter')


class FruitSegmentron():
    Coordinates = None
    Masks = []

    def __init__(self) -> None:
        print('initialising detectron')
        config_file = 'COCO-InstanceSegmentation/mask_rcnn_R_101_FPN_3x.yaml'
        classes = ['Tomato fruit']

        cfg = get_cfg()
        cfg.merge_from_file(model_zoo.get_config_file(config_file))
        # cfg.DATASETS.TRAIN = ("train",)
        # cfg.DATASETS.TEST = ("val",)

        cfg.NUM_GPUS = 1
        cfg.DATALOADER.NUM_WORKERS = 2
        cfg.MODEL.WEIGHTS = os.path.join(WEIGHTSFOLDER, 'segmentation.pth')
        cfg.MODEL.DEVICE = 'cpu'
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(classes)

        cfg.INPUT.MAX_SIZE_TRAIN = 1280
        cfg.INPUT.MAX_SIZE_TEST = 1280

        cfg.INPUT.MIN_SIZE_TRAIN = 720
        cfg.INPUT.MIN_SIZE_TEST = 720

        cfg.OUTPUT_DIR = WEIGHTSFOLDER
        os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)

        cfg.OUTPUT_DIR = WEIGHTSFOLDER  ##normally

        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.8  # set the testing threshold for this model
        cfg.MODEL.ROI_HEADS.NMS_THRESH_TEST = 0.5

        self.iou_th = cfg.MODEL.ROI_HEADS.NMS_THRESH_TEST

        print('initialising predictor')
        self.predictor = DefaultPredictor(cfg)
        print('detectron network loaded')

    def process_single(self, file_name):
        """Processing a single image, input is str file_name"""

        img = cv2.imread(file_name)
        t1 = time.time()
        outputs = self.predictor(img)
        print(time.time() - t1)

        self.m = np.zeros(outputs['instances'].pred_masks.cpu().shape[1:])
        dim = (160, 160)

        self.Masks = outputs['instances'].pred_masks.numpy()

        self.Coordinates = np.zeros([len(outputs['instances']), 5])
        for i in range(len(outputs['instances'])):
            m_ = np.asarray(outputs['instances'].pred_masks.cpu()[i, :, :])
            self.m += (i + 1) * m_

            # self.Masks.append(outputs['instances'].pred_masks.numpy()[i])

            Cluster = cv2.connectedComponentsWithStats(np.uint8(m_), 8, cv2.CV_32S)
            self.Coordinates[i, :] = Cluster[2][1, :]

        imRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        imRGB_masked = cv2.bitwise_and(imRGB, imRGB, mask=np.uint8(self.m != 0))
        self.process_coordinates()

        count_up, count_down, count_side = 0, 0, 0
        ResultsDict = {'Images': [], 'Keys': [], 'Coordinates': [], 'Masks': self.Masks}
        # ResultsDict = {'Images': [], 'Keys': [], 'Coordinates': [], 'Masks': []}

        for i in range(self.Coordinates.shape[0]):
            C = np.zeros(4, dtype=np.uint16)
            C[0] = self.Coordinates[i, 0] - 5
            C[1] = self.Coordinates[i, 1] - 5
            C[2] = self.Coordinates[i, 2] + 10
            C[3] = self.Coordinates[i, 3] + 10

            # ResultsDict['Masks'].append(np.uint8(self.m[(C[1] + 5):(C[3] - 10 + C[1] + 5), (C[0] + 5):(C[2] - 10 + C[0] + 5)]) != 0)

            im_c = imRGB_masked[C[1]:(C[1] + C[3]), C[0]:(C[0] + C[2]), :]
            m_c = self.m[C[1]:(C[1] + C[3]), C[0]:(C[0] + C[2])]

            RS = int((dim[0] - im_c.shape[0]) / 2)
            CS = int((dim[0] - im_c.shape[1]) / 2)

            im_ = np.zeros([dim[0], dim[1], 3], dtype=np.uint8)

            if (RS > 0) & (CS > 0):
                im_[RS:(RS + im_c.shape[0]), CS:(CS + im_c.shape[1]), :] = im_c
            elif (RS <= 0) & (CS > 0):
                im_[:, CS:(CS + im_c.shape[1]), :] = im_c[abs(RS):(abs(RS) + 160), :, :]
            elif (RS > 0) & (CS <= 0):
                im_[RS:(RS + im_c.shape[0]), :, :] = im_c[:, abs(CS):(abs(CS) + 160), :]
            elif (RS <= 0) & (CS <= 0):
                im_ = im_c[abs(RS):(abs(RS) + 160), abs(CS):(abs(CS) + 160), :]

            if self.Coordinates[i, 5] == 0:
                ResultsDict['Keys'].append('up_' + str(count_up))
                count_up += 1
            elif self.Coordinates[i, 5] == 1:
                ResultsDict['Keys'].append('side_' + str(count_side))
                count_side += 1
            elif self.Coordinates[i, 5] == 2:
                ResultsDict['Keys'].append('down_' + str(count_down))
                count_down += 1

            ResultsDict['Images'].append(im_)
            ResultsDict['Coordinates'].append(C)

        return ResultsDict

    def process_coordinates(self):
        index = np.argsort(self.Coordinates[:, 0])
        print(index)
        self.Coordinates = self.Coordinates[index, :]
        self.Masks = self.Masks[index]

        Rows = np.linspace(0,
                           self.Coordinates.shape[0],
                           int(self.Coordinates.shape[0] / 3 + 1),
                           dtype=np.uint8)

        for i in range(len(Rows) - 1):
            row_index = np.argsort(self.Coordinates[Rows[i]:Rows[i + 1], 1])
            self.Coordinates[Rows[i]:Rows[i + 1], :] = self.Coordinates[
                                                       row_index + Rows[i],
                                                       :]
            self.Masks[i * 3: i * 3 + 3] = self.Masks[i * 3: i * 3 + 3][row_index]

        self.Coordinates = np.c_[self.Coordinates, [0, 1, 2] * np.uint8(self.Coordinates.shape[0] / 3)]

if __name__ == '__main__':
    file = os.path.join(os.path.dirname(__file__), 'test', 'D415_test1_1_single_rgb_raw.png')

    segmenter = FruitSegmentron()
    tomato_list = segmenter.process_single(file)

    tomatos = []

    for idx, key in enumerate(tomato_list['Keys']):
        orientation = key.split('_')[0]
        tomato = {
            'id': idx + 1,
            'properties': {
                'orientation': orientation,
            },
            'traits': {}
        }

        tomatos.append(tomato)

    print(tomatos)
