import os

import cv2
import numpy as np
import open3d as o3d
import pyrealsense2 as rs
from kivy.app import App
from kivymd.uix.label import MDLabel


class RealsenseCapture:
    depth_scale = None
    profile = None
    app = App.get_running_app()

    def __init__(self):
        # Configure depth and color streams
        self.pipeline = rs.pipeline()
        self.config = rs.config()

        self.color_camera_options = {
            # rs.option.auto_exposure_priority: True,
            # rs.option.enable_auto_exposure: True,
            rs.option.exposure: 150,  # microseconds, max 165,000
            rs.option.enable_auto_white_balance: True,
            # rs.option.white_balance: 5400
        }

        self.depth_camera_options = {
            rs.option.enable_auto_exposure: True,
            # rs.option.exposure: 3000, # max 165,000
            # rs.option.gain: 85,
            # rs.option.laser_power: 150,
            # rs.option.inter_cam_sync_mode: 1,
            # rs.option.output_trigger_enabled: True
        }

        # Get device product line for setting a supporting resolution
        try:
            pipeline_wrapper = rs.pipeline_wrapper(self.pipeline)
            pipeline_profile = self.config.resolve(pipeline_wrapper)

            self.device = pipeline_profile.get_device()
            self.device_product_line = str(self.device.get_info(rs.camera_info.product_line))

            self.config.enable_stream(rs.stream.depth, 1280, 720, rs.format.z16, 30)
            # self.config.enable_stream(rs.stream.color, 1280, 720, rs.format.bgr8, 30)
            self.config.enable_stream(rs.stream.color, 1920, 1080, rs.format.bgr8, 30)

        except RuntimeError as e:
            print(e)

    def start(self):
        # Start streaming
        self.profile = self.pipeline.start(self.config)

        color_sensor = self.profile.get_device().first_color_sensor()
        for setting, value in self.color_camera_options.items():
            color_sensor.set_option(setting, value)
            # time.sleep(1)  # short delay for processing

        depth_sensor = self.profile.get_device().first_depth_sensor()
        for setting, value in self.depth_camera_options.items():
            depth_sensor.set_option(setting, value)
            # time.sleep(1)  # short delay for processing
        self.depth_scale = depth_sensor.get_depth_scale()

        print('Starting RealSense pipeline')

    def read(self):
        # Flag capture available
        ret = True

        # get frames
        frames = self.pipeline.wait_for_frames()

        # separate RGB and Depth image
        color_frame = frames.get_color_frame()  # RGB
        depth_frame = frames.get_depth_frame()  # Depth

        depth = np.asanyarray(depth_frame.get_data())
        depth = depth * self.depth_scale
        dist, _, _, _ = cv2.mean(depth)

        screen = self.app.get_screen('Snap')
        screen.ids.warning.clear_widgets()

        # if dist < 0.5:
        #     screen.ids.warning.add_widget(
        #         MDLabel(
        #             text='Objects are too close'
        #         )
        #     )
        #
        # dist_to_center = depth_frame.get_distance(width / 2, height / 2)
        # print(dist_to_center)

        if not color_frame or not depth_frame:
            ret = False
            return ret, None

        # Convert images to numpy arrays
        # depth_image = np.asanyarray(depth_frame.get_data())
        color_image = np.asanyarray(color_frame.get_data())

        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
        # depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

        # depth_colormap_dim = depth_colormap.shape
        # color_colormap_dim = color_image.shape

        # If depth and color resolutions are different, resize color image to match depth image for display
        # if depth_colormap_dim != color_colormap_dim:
        #     resized_color_image = cv2.resize(color_image, dsize=(depth_colormap_dim[1], depth_colormap_dim[0]),
        #                                      interpolation=cv2.INTER_AREA)
        #     images = np.hstack((resized_color_image, depth_colormap))
        # else:
        #     images = np.hstack((color_image, depth_colormap))

        # return ret, images

        return ret, color_image

    def snapshot(self):
        images = self.get_frames()
        # images = self.get_images(frames)

        if not os.path.exists('assets/temp'):
            os.mkdir('assets/temp')

        for key, image in images.items():
            if key == 'points':
                filename = key + '.ply'
                o3d.io.write_point_cloud(os.path.join('assets', 'temp', filename), image)  # Write the point cloud
            else:
                filename = key + '.png'
                cv2.imwrite(os.path.join('assets', 'temp', filename), image)

        return

    def get_frame(self, align):
        frames = self.pipeline.wait_for_frames()

        # align the depth to the RGB camera
        aligned_frames = align.process(frames)

        depth_frame = aligned_frames.get_depth_frame()
        color_frame = aligned_frames.get_color_frame()
        color_frame_raw = frames.get_color_frame()

        # ir_l = aligned_frames.get_infrared_frame(1)
        # ir_r = aligned_frames.get_infrared_frame(2)

        return {
            'depth': depth_frame,
            'rgb_aligned': color_frame,
            'rgb_raw': color_frame_raw,
            # "irL": ir_l,
            # "irR": ir_r
        }

    def get_frames(self, frame_count=60):
        align = rs.align(rs.stream.depth)

        p = self.profile.get_stream(rs.stream.depth)
        intrinsics = p.as_video_stream_profile().get_intrinsics()

        for i in range(frame_count):  # take num_images consecutive images
            frames = self.get_frame(align)
            images = self.get_images(frames)

            D = np.asanyarray(frames['depth'].get_data())
            D = np.float32(D.reshape([np.prod(D.shape)])) * self.depth_scale

            if i == 0:
                depth = np.zeros(images['depth'].shape)
                rgb_raw = np.zeros(images['rgb_raw'].shape)
                rgb_aligned = np.zeros(images['rgb_aligned'].shape)

                # if config.camera_type == 'l515':
                #     IR = np.zeros(images['ir'].shape)
                # elif config.camera_type == 'd415':
                #     IR_L = np.zeros(images['irL'].shape)
                #     IR_R = np.zeros(images['irR'].shape)

                normalise = np.zeros([depth.shape[0], depth.shape[1]])

            # Sum all the frames of num_images
            depth += images['depth']
            rgb_raw += images['rgb_raw']
            rgb_aligned += images['rgb_aligned']

            # if config.camera_type == 'l515':
            #     IR += images['ir']
            # elif config.camera_type == 'd415':
            #     IR_L += images['irL']
            #     IR_R += images['irR']

            # Save how many data points were truly summed for each pixel
            normalise += (images['depth'] != 0)

        # Avoid dividing by zero when normalizing
        normalise[normalise == 0] = 1

        # Normalize each summed pixel value to the number of non-zero values summed
        depth /= normalise
        rgb_aligned /= np.moveaxis(np.array([normalise, normalise, normalise]), 0, 2)
        depth[normalise < frame_count / 4] = 0

        # Get a list of vertices, instead of depth image
        vtx = np.zeros([np.prod(depth.shape), 3])

        n = 0
        for i in range(depth.shape[0]):
            for j in range(depth.shape[1]):
                vtx[n, :] = rs.rs2_deproject_pixel_to_point(
                    intrinsics, [j, i], depth[i, j] * self.depth_scale)
                n += 1

        # As a final product, we need a list coordinates
        pcd = o3d.geometry.PointCloud()
        pcd.points = o3d.utility.Vector3dVector(vtx)

        im = rgb_aligned.copy()
        im_ = np.float64(im.reshape([np.prod(im.shape[:2]), im.shape[2]]))
        im_ /= 255
        pcd.colors = o3d.utility.Vector3dVector(im_)

        images['depth'] = np.uint16(depth)
        images['rgb_raw'] = np.uint8(rgb_raw / frame_count)
        images['rgb_aligned'] = np.uint8(rgb_aligned)
        images['points'] = pcd

        # if config.camera_type == 'l515':
        #     images['ir'] = np.uint16((2 ** 16 - 1) * IR / np.max(IR))
        # elif config.camera_type == 'd415':
        #     images['irL'] = np.uint16((2 ** 16 - 1) * IR_L / np.max(IR_L))
        #     images['irR'] = np.uint16((2 ** 16 - 1) * IR_L / np.max(IR_L))

        return images

    def get_images(self, frames):
        # convert the frames to numpy arrays
        images = {}
        for key, frame in frames.items():
            image = np.asanyarray(frame.get_data())

            # if key == 'depth':
            #     image = cv2.applyColorMap(cv2.convertScaleAbs(image, alpha=0.03), cv2.COLORMAP_JET)

            images[key] = image

        return images

    def release(self):
        # Stop streaming
        self.pipeline.stop()
