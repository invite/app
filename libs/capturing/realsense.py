import cv2
import numpy as np
import pyrealsense2 as rs

from datetime import datetime


class RealSense:

    def __init__(self):
        print('RealSense instance created')

        # Configure depth and color streams
        self.pipeline = rs.pipeline()
        self.config = rs.config()

        # Get device product line for setting a supporting resolution
        pipeline_wrapper = rs.pipeline_wrapper(self.pipeline)
        pipeline_profile = self.config.resolve(pipeline_wrapper)

        self.device = pipeline_profile.get_device()
        self.device_product_line = str(self.device.get_info(rs.camera_info.product_line))

        self.config.enable_stream(rs.stream.depth, 1280, 720, rs.format.z16, 30)
        # self.config.enable_stream(rs.stream.color, 1920, 1080, rs.format.bgr8, 30)
        self.config.enable_stream(rs.stream.color, 1280, 720, rs.format.bgr8, 30)
        # self.config.enable_stream(rs.stream.infrared, 1, 640, 480, rs.format.y8, 6)
        # self.config.enable_stream(rs.stream.infrared, 2, 640, 480, rs.format.y8, 6)

    def stream(self):
        self.pipeline.start(self.config)

    def get_frames(self, align):
        frames = self.pipeline.wait_for_frames()

        # align the depth to the RGB camera
        aligned_frames = align.process(frames)

        depth_frame = aligned_frames.get_depth_frame()
        color_frame = aligned_frames.get_color_frame()
        color_frame_raw = frames.get_color_frame()

        # ir_l = aligned_frames.get_infrared_frame(1)
        # ir_r = aligned_frames.get_infrared_frame(2)

        return {
            "depth": depth_frame,
            "rgb_aligned": color_frame,
            "rgb_raw": color_frame_raw,
            # "irL": ir_l,
            # "irR": ir_r
        }

    def get_images(self, frames):
        # if self.config.first_image == True:
        #     self.config.first_image = False

        # convert the frames to numpy arrays
        images = {}
        for key, frame in frames.items():
            image = np.asanyarray(frame.get_data())
            images[key] = image

        return images

    def frame(self):
        align = rs.align(rs.stream.depth)

        # profile, depth_scale = start_pipeline()
        frames = self.get_frames(align)
        images = self.get_images(frames)

        timestamp = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

        for key, image in images.items():
            # cv2.imwrite("data/" + timestamp + "_" + key + ".png", image)
            cv2.imwrite("../data/invite_" + key + ".png", image)

