import math
import os
import shutil
from datetime import datetime

from kivy.app import App
from kivy.core.window import Window
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.vkeyboard import VKeyboard

from libs.baseclass.batch_card import BatchCard


class Trial(Screen):
    app = App.get_running_app()
    trial = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(Trial, self).__init__(**kwargs)

    def on_enter(self, *args):
        self.trial = self.app.trial

        self.ids['toolbar'].title = self.trial['name']
        self.ids.gallery.clear_widgets()
        self.load_batches(self.ids.gallery)

    def load_batches(self, element=None):
        if element is None:
            element = self.ids['gallery']

        trial_path = os.path.join(self.app.trial_path, self.trial['name'].lower())

        batch_files = [batch for batch in os.listdir(trial_path) if os.path.isdir(os.path.join(trial_path, batch))]
        batch_files.sort(key=lambda x: int(x))

        # Card dimensions are 240x180
        element.cols = math.floor(Window.size[0] / 240)
        element.rows = math.ceil(len(batch_files) / element.cols)

        # Adjust the default width for an even spread
        element.col_default_width = (Window.size[0] - element.spacing[0] * element.cols) / element.cols

        for batch in batch_files:
            element.add_widget(
                BatchCard(
                    line_color=(0.2, 0.2, 0.2, 0.8),
                    focus_behavior=True,
                    radius=['6dp', '6dp', '6dp', '6dp'],
                    elevation=5,
                    trial=self.app.trial,
                    batch=batch
                )
            )