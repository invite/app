import json
import math
import os
import shutil
from datetime import datetime

from kivy.app import App
from kivy.core.window import Window
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.vkeyboard import VKeyboard

from libs.baseclass.batch_card import BatchCard
from libs.baseclass.instance_card import InstanceCard


class Batch(Screen):
    app = App.get_running_app()
    trial = ObjectProperty(None)
    batch = StringProperty(None)

    def __init__(self, **kwargs):
        super(Batch, self).__init__(**kwargs)

    def on_enter(self, *args):
        self.trial = self.app.trial
        self.batch = self.app.batch

        self.ids.toolbar.title = self.trial['name'] + ' / ' + self.batch
        self.ids.raw.source = os.path.join(self.app.trial_path, self.trial['name'].lower(), self.batch,
                                           'raw', 'rgb_raw.png')

        self.load_instances()

    def load_instances(self):
        batch_path = os.path.join(self.app.trial_path, self.trial['name'].lower(), self.batch)
        with open(os.path.join(batch_path, '.instances'), 'r') as f:
            tomatoes = json.load(f)

        instances = {
            'up': [],
            'down': [],
            'side': []
        }

        for tomato in tomatoes:
            orientation = tomato['properties']['orientation']
            instances[orientation].append(tomato)

        for key, i_list in instances.items():
            element = self.ids[key]
            element.clear_widgets()
            # Card dimensions are 240x180
            # element.cols = math.floor(Window.size[0] / 240)
            # element.rows = max(math.ceil(len(i_list) / element.cols), 1) + 1

            # Adjust the default width for an even spread
            # element.col_default_width = (Window.size[0] - element.spacing[0] * element.cols) / element.cols

            for instance in i_list:
                element.add_widget(
                    InstanceCard(
                        line_color=(0.2, 0.2, 0.2, 0.8),
                        focus_behavior=True,
                        radius=['6dp', '6dp', '6dp', '6dp'],
                        elevation=5,
                        instance=instance
                    )
                )
