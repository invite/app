from kivy.properties import StringProperty, ObjectProperty

from kivymd.uix.behaviors import RoundedRectangularElevationBehavior
from kivymd.uix.card import MDCard


class Card(MDCard, RoundedRectangularElevationBehavior):
    """Implements a material design v3 card."""
    text = StringProperty()
    trial = ObjectProperty()

