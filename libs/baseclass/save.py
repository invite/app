import os
import shutil
from datetime import datetime
from pathlib import Path

import numpy as np

import cv2
from kivy.app import App
from kivy.core.window import Window
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.vkeyboard import VKeyboard
from matplotlib import pyplot as plt

from libs.vision.segmenter import Segmentron


class Save(Screen):
    batch = ObjectProperty(None)
    app = App.get_running_app()

    def __init__(self, **kwargs):
        super(Save, self).__init__(**kwargs)
        Window.bind(on_key_down=self._on_keyboard_down)

    def _on_keyboard_down(self, instance, keyboard, keycode, text, modifiers):
        if self.batch.focus and keycode == 40:  # 40 - Enter key pressed
            self.ids.button.trigger_action()

    def on_pre_enter(self, *args):
        batch_path = os.path.join('assets', 'trials', self.app.trial['name'].lower())
        batches = len(os.listdir(batch_path))

        self.save(str(batches))

        if 'image' in self.ids:
            self.ids.image.reload()
        if 'batch' in self.ids:
            self.ids.batch.text = ''

    def show_keyboard(self):
        return
        # os.system('C:\\PROGRA~1\\COMMON~1\\MICROS~1\\ink\\tabtip.exe')
        # print('Loading Keyboard')
        # kb = VKeyboard()
        # self.ids.save.add_widget(kb)

    def save(self, batch):
        self.app.transition('Process', self.app.trial, batch)

        # dl = Segmentron()
        # outputs, bgr_output = dl.process_single(os.path.join(raw_path, 'rgb_raw.png'))
        #
        # print(outputs)
        # # plt.imshow(bgr_output[:, :, ::-1])
        # # plt.show()
        #
        # img = cv2.imread(os.path.join(raw_path, 'rgb_raw.png'))
        # # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        #
        # instance_path = os.path.join(batch_path, 'instances')
        # os.makedirs(instance_path, exist_ok=True)
        #
        # for idx, mask in enumerate(outputs['instances'].pred_masks.numpy()):
        #     orientation = outputs['instances'].pred_classes[idx].numpy()
        #     img_mask = np.uint8(np.where(mask, 255, 0))
        #
        #     (y, x) = np.where(img_mask == 255)
        #     (topy, topx) = ((np.min(y) - 8), (np.min(x) - 8))
        #     (bottomy, bottomx) = ((np.max(y) + 8), (np.max(x) + 8))
        #     masked = cv2.bitwise_and(img, img, mask=img_mask)
        #     cropped = masked[topy:bottomy + 1, topx:bottomx + 1]
        #
        #     cv2.imwrite(os.path.join(instance_path, str(idx + 1) + '.' + classes[orientation] + '.png'), cropped)
        #
        # self.app.transition('Trial')
