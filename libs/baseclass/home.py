import json
import os
import re
from pathlib import Path

import kivymd
from kivy.app import App
from kivy.properties import ListProperty
from kivy.uix.screenmanager import Screen
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDFlatButton
from kivymd.uix.dialog import MDDialog
from kivymd.uix.textfield import MDTextField

from libs.baseclass.card import Card


class DialogContent(MDBoxLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.register_event_type('on_submit')

    def on_submit(self):
        children = [c for c in self.children if isinstance(c, kivymd.uix.textfield.MDTextField)]
        for child in children:
            # Focus the field in order to display errors
            child.focus = True
            # Remove focus to prevent the error from disappearing
            child.focus = False

        return children

    def submit(self, instance):
        if instance.required and not instance.text:
            instance.error = True

    def validate(self, instance, group):
        if group == 'none':
            return

        if group == 'empty':
            if instance.text is None:
                instance.error = True
            elif instance.text == '':
                instance.error = True
            else:
                instance.error = False

        if group == 'date':
            if instance.text is None or instance.text == '':
                instance.error = True
                return

            match = re.search(
                "^([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])(-)([1-9]|0[1-9]|1[0-2])(-)(19[0-9][0-9]|20[0-9][0-9])$",
                instance.text)
            if match is None:
                instance.error = True
            else:
                instance.error = False


class Home(Screen):
    # TODO: Windows Home Directory
    # home = str(Path.home())

    app = App.get_running_app()
    dialog = None

    def on_leave(self, *args):
        self.ids.list.clear_widgets()

    def on_enter(self, *args):
        if 'list' in self.ids:
            self.load_trials(self.ids.list)

    def create_trial(self):
        if not self.dialog:
            self.dialog = MDDialog(
                title='Create new trial',
                type='custom',
                content_cls=DialogContent(),
                buttons=[
                    MDFlatButton(
                        text='Create',
                        theme_text_color='Custom',
                        on_release=lambda _: self.submit_trial()
                    ),
                    MDFlatButton(
                        text='Cancel',
                        theme_text_color='Custom',
                        on_release=lambda _: self.dialog.dismiss()
                    ),
                ],
            )
        self.dialog.open()

    def submit_trial(self):
        fields = self.dialog.content_cls.dispatch('on_submit')
        valid = True

        for field in fields:
            if field.error:
                valid = False

        valid = True

        if not valid:
            # self.dialog.content_cls.ids['valid'].text = 'Form is not valid, check for errors.'
            pass
        else:
            trial_data = {}
            labels = {
                'name_field': 'name',
                'desc_field': 'description',
                'date_field': 'date'
            }

            for field in self.dialog.content_cls.ids:
                label = labels[field]
                trial_data[label] = self.dialog.content_cls.ids[field].text

            trial_path = os.path.join(self.app.trial_path, trial_data['name'].lower())
            os.makedirs(trial_path, exist_ok=True)
            with open(os.path.join(trial_path, '.settings'), 'w') as f:
                json.dump(trial_data, f)

            self.load_trials()
            self.dialog.dismiss()

    def load_trials(self, element=None):
        if element is None:
            element = self.ids['list']

        element.clear_widgets()

        for trial in os.listdir(self.app.trial_path):
            if os.path.isfile(os.path.join(self.app.trial_path, trial, '.settings')):
                with open(os.path.join(self.app.trial_path, trial, '.settings')) as f:
                    trial = json.load(f)

                # element.clear_widgets()
                element.add_widget(
                    Card(
                        line_color=(0.2, 0.2, 0.2, 0.8),
                        focus_behavior=True,
                        radius=['6dp', '6dp', '6dp', '6dp'],
                        elevation=5,
                        text=trial['name'],
                        trial=trial
                    )
                )
