import os.path

from kivy.app import App
from kivy.properties import StringProperty, ObjectProperty

from kivymd.uix.behaviors import RoundedRectangularElevationBehavior
from kivymd.uix.card import MDCard


class BatchCard(MDCard, RoundedRectangularElevationBehavior):
    """Implements a material design v3 card."""
    app = App.get_running_app()
    trial = ObjectProperty()
    batch = StringProperty()

    def __init__(self, **kwargs):
        super(BatchCard, self).__init__(**kwargs)

        self.ids.image.source = os.path.join(self.app.trial_path, self.trial['name'].lower(), self.batch,
                                                'raw', 'rgb_raw.png')
        self.ids.batch.text = 'Batch: ' + str(self.batch)

