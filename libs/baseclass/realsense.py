import cv2

from kivy.clock import Clock
from kivy.graphics.texture import Texture
from kivy.uix.image import Image

from libs.capturing import RealsenseCapture


class RealSense(Image):
    def __init__(self, **kwargs):
        super(RealSense, self).__init__(**kwargs)
        self.capture = RealsenseCapture()
        self.capture.start()

        Clock.schedule_interval(self.update, 1.0 / 60)

    def update(self, dt):
        ret, frame = self.capture.read()

        if ret:
            # convert it to texture
            buf1 = cv2.flip(frame, 0)
            buf = buf1.tostring()
            image_texture = Texture.create(size=(frame.shape[1], frame.shape[0]), colorfmt='bgr')
            image_texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')
            # display image from the texture
            self.texture = image_texture

    def snapshot(self):
        self.capture.snapshot()






