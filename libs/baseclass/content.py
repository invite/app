import os

from kivy.properties import StringProperty
from kivy.uix.image import Image
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.gridlayout import MDGridLayout


class Content(MDGridLayout):
    value = StringProperty()

    def __init__(self, value, **kwargs):
        super().__init__(**kwargs)

        rgb = Image(source=os.path.join('assets', 'snapshots', value, 'rgb_raw.png'))
        rgb.keep_ratio = True
        rgb.height = '200dp'

        depth = Image(source=os.path.join('assets', 'snapshots', value, 'depth.png'))
        depth.keep_ratio = True
        depth.height = '200dp'

        self.add_widget(rgb)
        self.add_widget(depth)