import json
import os
import pickle
import shutil
import weakref
from threading import Thread

import numpy as np

import cv2
import open3d as o3d
from PIL import Image

from kivy.app import App
from kivy.properties import StringProperty
from kivy.uix.screenmanager import Screen
from kivymd.uix.label import MDLabel
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split

# from libs.vision.segmenter import Segmentron
from libs.vision.peduncle import PeduncleSegmentron
from libs.vision.tray import FruitSegmentron


class Process(Screen):
    app = App.get_running_app()

    def __init__(self, **kwargs):
        super(Process, self).__init__(**kwargs)

    def on_enter(self, *args):
        # self.ids.save.text = 'Storing source files...'
        # self.ids.segment.text = ''
        # self.ids.count.text = ''

        self.ids.status.clear_widgets()
        for label_id in ['save', 'segment', 'distance', 'peduncle', 'hue', 'shape']:
            label = MDLabel(size_hint=(1, None), height='12dp')

            self.ids.status.add_widget(label)
            self.ids[label_id] = weakref.ref(label)

        self.save()

    def save(self):
        self.ids.save.text = 'Storing source files...'

        source = 'assets/temp'
        batch = self.app.batch

        print(self.app.trial)

        batch_path = os.path.join('assets', 'trials', self.app.trial['name'].lower(), batch)
        raw_path = os.path.join(batch_path, 'raw')
        os.makedirs(raw_path, exist_ok=True)

        for file_name in os.listdir(source):
            # construct full file path
            source_file = os.path.join(source, file_name)
            destination_file = os.path.join(raw_path, file_name)

            # move only files
            if os.path.isfile(source_file):
                shutil.move(source_file, destination_file)

        # self.ids.save.text = 'Storing source files... Done.'

        Thread(target=Segmenter().process).start()


class Segmenter:
    app = App.get_running_app()
    min_size = 50

    def __init__(self, **kwargs):
        super(Segmenter, self).__init__(**kwargs)

    def process(self, trial=None, batch=None):
        screen = self.app.get_screen('Process')
        tomato_list = []

        if trial is None:
            trial = self.app.trial

        if batch is None:
            batch = self.app.batch

        batch_path = os.path.join('assets', 'trials', trial['name'].lower(), batch)
        # batch_path = os.path.join('..', '..', 'assets', 'trials', trial, batch)

        raw_path = os.path.join(batch_path, 'raw')
        instance_path = os.path.join(batch_path, 'instances')
        os.makedirs(raw_path, exist_ok=True)
        os.makedirs(instance_path, exist_ok=True)

        screen.ids.segment.text = 'Extracting tomatoes...'

        segmenter = FruitSegmentron()
        peduncler = PeduncleSegmentron()

        segmentation = segmenter.process_single(os.path.join(raw_path, 'rgb_aligned.png'))
        # segmentation = segmenter.process_single(test_path)

        img = cv2.imread(os.path.join(raw_path, 'rgb_aligned.png'))

        points = o3d.io.read_point_cloud(os.path.join(raw_path, 'points.ply'))
        Coordinates = np.asarray(points.points).reshape(img.shape)

        for idx, key in enumerate(segmentation['Keys']):
            mask = segmentation['Masks'][idx]
            orientation = key.split('_')[0]

            cv2.imwrite(os.path.join(instance_path, str(idx + 1) + '.png'),
                        cv2.cvtColor(segmentation['Images'][idx], cv2.COLOR_BGR2RGB))

            tomato = {
                'id': idx + 1,
                'properties': {
                    'orientation': orientation,
                    'trial': trial['name'],
                    # 'trial': trial,
                    'batch': batch,
                },
                'traits': {}
            }

            img_mask = np.uint8(np.where(mask, 255, 0))

            CoordMasked = cv2.bitwise_and(Coordinates, Coordinates, mask=img_mask)

            (y, x) = np.where(img_mask == 255)
            (topy, topx) = ((np.min(y) - 8), (np.min(x) - 8))
            (bottomy, bottomx) = ((np.max(y) + 8), (np.max(x) + 8))
            masked = cv2.bitwise_and(img, img, mask=img_mask)

            cropped = masked[topy:bottomy + 1, topx:bottomx + 1]
            cv2.imwrite(
                os.path.join(instance_path, str(idx + 1) + '.cropped.png'),
                cropped)

            croppedCoords = CoordMasked[topy:bottomy + 1, topx:bottomx + 1]

            screen.ids.distance.text = 'Calculating distance from camera...'

            mmperpix_x = np.zeros(10)
            mmperpix_y = np.zeros(10)

            for j, line in enumerate(np.linspace(0.25, 0.75, 10)):
                Cut = croppedCoords[int(line * croppedCoords.shape[0]), :, 0]
                X = np.where(Cut != 0)[0]
                Y = Cut[np.where(Cut != 0)[0]]
                model = LinearRegression()
                model.fit(X.reshape([len(X), 1]), Y.reshape([len(Y), 1]))
                mmperpix_x[j] = np.round(1000 * model.coef_[0][0], 3)

                Cut = croppedCoords[:, int(line * croppedCoords.shape[1]), 1]
                X = np.where(Cut != 0)[0]
                Y = Cut[np.where(Cut != 0)[0]]
                model = LinearRegression()
                model.fit(X.reshape([len(X), 1]), Y.reshape([len(Y), 1]))
                mmperpix_y[j] = np.round(1000 * model.coef_[0][0], 3)

            mmperpix = np.round(np.mean([mmperpix_x, mmperpix_y]), 3)
            tomato['properties']['mmmPerPixel'] = mmperpix

            peduncle_mask = None
            if orientation == 'up':
                screen.ids.peduncle.text = 'Scoring peduncles scar size...'

                ped = peduncler.process_single(cv2.cvtColor(segmentation['Images'][idx], cv2.COLOR_RGB2BGR))
                peduncle_mask = ped

                pixels = np.count_nonzero(ped)
                tomato['traits']['peduncle'] = {
                    'pixels': int(pixels),
                    'mm2': round(int(pixels) * (mmperpix * mmperpix), 2),
                }

                im_copy = np.float64(segmentation['Images'][idx].copy())
                im_copy[:, :, 0][ped] *= 0.6
                im_copy[:, :, 1][ped] *= 0.6
                im_copy[:, :, 2][ped] *= 2

                im_copy[im_copy > 255] = 255
                im_copy[im_copy < 0] = 0

                # plt.imshow(np.uint8(im_copy))
                # plt.show()
                # plt.imshow(segmentation['Images'][idx])
                # plt.show()

                converted = cv2.cvtColor(np.uint8(im_copy), cv2.COLOR_BGR2RGB)

                cv2.imwrite(os.path.join(instance_path, str(idx + 1) + '.peduncle.png'), converted)

            screen.ids.hue.text = 'Calculating Hue...'

            if orientation == 'up':
                Kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7))
                HSV = cv2.cvtColor(segmentation['Images'][idx], cv2.COLOR_BGR2HSV)
                mask = peduncle_mask

                mask = (mask == 0) * (HSV[:, :, 2] != 0)
                mask = cv2.erode(np.uint8(mask), kernel=Kernel)

                HueFlesh = cv2.bitwise_and(HSV, HSV, mask=mask)[:, :, 0]
                HueFlesh = np.uint8(np.float64(HueFlesh) / (179 / 255)) + 64

                tomato['traits']['color'] = {
                    'hue': float(np.mean(HueFlesh[mask != 0]))
                }

            else:
                HSV = cv2.cvtColor(segmentation['Images'][idx], cv2.COLOR_BGR2HSV)
                HueFlesh = HSV[:, :, 0]
                HueFlesh = np.uint8(np.float64(HueFlesh) / (179 / 255)) + 64

                tomato['traits']['color'] = {
                    'hue': float(np.mean(HueFlesh[HSV[:, :, 2] != 0]))
                }

            screen.ids.shape.text = 'Determining Shape and Volume...'
            Kernel1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))

            im = segmentation['Images'][idx].copy()

            m = np.uint8(np.sum(segmentation['Images'][idx].copy(), axis=2) != 0)
            Clusters = cv2.connectedComponentsWithStats(m, 8, cv2.CV_32S)
            mask = np.uint8(Clusters[1] == (np.argmax(Clusters[2][1:, 4]) + 1))

            # mask = np.uint8(np.sum(m_new, axis=2) != 0)
            mask = cv2.erode(mask, kernel=Kernel1)
            mask = cv2.dilate(mask, kernel=Kernel1)
            mask = cv2.dilate(mask, kernel=Kernel1)
            mask = cv2.erode(mask, kernel=Kernel1)

            cnt = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            ellipse = cv2.fitEllipse(cnt[0][0])

            anglesLA = [ellipse[2], ellipse[2] - 180]
            anglesSA = [(ellipse[2] + 90) % 180 - (ellipse[2] >= 90) * 180,
                        ellipse[2] - 90]

            if orientation == 'side':  # Find where the peduncle is
                cntCoord = np.zeros([cnt[0][0].shape[0], 3])
                cntCoord[:, :2] = cnt[0][0][:, 0, :]

                for i in range(cnt[0][0].shape[0]):
                    Vec = cntCoord[i, :2] - ellipse[0]
                    unitVec = Vec / np.linalg.norm(Vec)
                    dotProd = np.dot(unitVec, [0, 1])
                    cntCoord[i, 2] = -np.sign(unitVec[0]) * (np.arccos(dotProd))

                ellipseCnt = np.where(cv2.ellipse(np.zeros(im.shape),
                                                  tuple(np.uint16(ellipse[0])),
                                                  tuple(np.uint16(np.array(ellipse[1]) / 2)),
                                                  ellipse[2], 0, 360, (0, 0, 255), 1)[:, :, 2] != 0)

                ellipseCoord = np.zeros([ellipseCnt[0].shape[0], 3])
                ellipseCoord[:, 0] = ellipseCnt[1]
                ellipseCoord[:, 1] = ellipseCnt[0]

                for i in range(ellipseCnt[0].shape[0]):
                    Vec = ellipseCoord[i, :2] - ellipse[0]
                    unitVec = Vec / np.linalg.norm(Vec)
                    dotProd = np.dot(unitVec, [0, 1])
                    ellipseCoord[i, 2] = -np.sign(unitVec[0]) * (np.arccos(dotProd))

                Angles = np.linspace(-np.pi, np.pi, 361)
                ellipseErrors = np.zeros(len(Angles))

                for i, angle in enumerate(Angles):
                    cntVec = cntCoord[np.argmin(abs(cntCoord[:, 2] - angle)), :2] - ellipse[0]
                    ellipseVec = ellipseCoord[np.argmin(abs(ellipseCoord[:, 2] - angle)), :2] - ellipse[0]
                    ellipseErrors[i] = np.sqrt(np.sum(cntVec ** 2)) - np.sqrt(np.sum(ellipseVec ** 2))

                critAngle = Angles[np.argmin(ellipseErrors)]
                critAngles = [(360 / (2 * np.pi)) * critAngle,
                              ((360 / (2 * np.pi)) * critAngle - 180)]

                if critAngles[1] < -180:
                    critAngles[1] = critAngles[1] % 180

                if np.min(ellipseErrors) < -min(ellipse[1]) / 18:
                    angleDiffs = np.zeros([2, 2, 2])

                    for h, angles in enumerate([anglesSA, anglesLA]):
                        for i in range(2):
                            for j in range(2):
                                angleDiffs[h, i, j] = abs(angles[i] - critAngles[j])

                    if list(set(np.where(angleDiffs == np.min(angleDiffs))[0]))[0] == 0:
                        tomato['traits']['dimensions'] = {
                            'height': ellipse[1][0] * mmperpix
                        }
                    elif list(set(np.where(angleDiffs == np.min(angleDiffs))[0]))[0] == 1:
                        tomato['traits']['dimensions'] = {
                            'height': ellipse[1][1] * mmperpix
                        }
                else:
                    tomato['traits']['dimensions'] = {
                        'height': ellipse[1][0] * mmperpix
                    }

            image = cv2.ellipse(im,
                                tuple(np.uint16(ellipse[0])),
                                tuple(np.uint16(np.array(ellipse[1]) / 2)),
                                ellipse[2], 0, 360, (0, 255, 0), 1)
            # image = cv2.drawContours(im, cnt[0], 0, (0, 255, 0), 1)

            if (orientation == 'up') | (orientation == 'down'):
                if 'dimensions' not in tomato['traits']:
                    tomato['traits']['dimensions'] = {}

                short_axis = ellipse[1][0] * mmperpix
                long_axis = ellipse[1][1] * mmperpix

                tomato['traits']['dimensions']['shortAxis'] = short_axis
                tomato['traits']['dimensions']['longAxis'] = long_axis

            converted = cv2.cvtColor(np.uint8(image), cv2.COLOR_BGR2RGB)
            cv2.imwrite(os.path.join(instance_path, str(idx + 1) + '.ellipse.png'), converted)

            tomato_list.append(tomato)

        short, long, height = 0, 0, 0
        short_count, long_count, height_count = 0, 0, 0

        for tomato in tomato_list:
            dimensions = tomato['traits']['dimensions']

            if 'height' in dimensions:
                height = height + dimensions['height']
                height_count = height_count + 1

            if 'shortAxis' in dimensions:
                short = short + dimensions['shortAxis']
                short_count = short_count + 1

            if 'longAxis' in dimensions:
                long = long + dimensions['longAxis']
                long_count = long_count + 1

        batch_volume = (1/6) * np.pi * (short / short_count) * (long / long_count) * (height / height_count)
        shape_ratio = np.mean([(short / short_count), (long / long_count)]) / (height / height_count)

        for tomato in tomato_list:
            tomato['traits']['dimensions']['batchVolume'] = batch_volume
            tomato['traits']['dimensions']['shapeRatio'] = shape_ratio

        with open(os.path.join(batch_path, '.instances'), 'w', encoding='utf-8') as outfile:
            json.dump(tomato_list, outfile)

        self.app.transition('Trial')

    def remove_small_clusters(self, Mask):
        new_mask = np.zeros([Mask.shape[0], Mask.shape[1]])
        # Find all the pixel clusters in the Carrots mask
        clusters = cv2.connectedComponentsWithStats(np.uint8(Mask), 8, cv2.CV_32S)
        clusters = list(clusters)

        # Find only clusters with a minimum size (SizeThreshold)
        object_idx = np.where(clusters[2][1:, 4] > self.min_size)[0] + 1
        if len(object_idx) > 0:
            for idx in object_idx:
                new_mask += clusters[1] == idx
        else:
            idx = np.argmax(clusters[2][1:, 4]) + 1
            new_mask += clusters[1] == idx

        return new_mask == 1


if __name__ == '__main__':
    file = os.path.join(os.path.dirname(__file__), 'test', 'D415_test1_1_single_rgb_raw.png')

    sgm = Segmenter()
    sgm.process('test', '101')
