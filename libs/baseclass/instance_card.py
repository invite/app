import os.path
from tkinter import Widget

from kivy.app import App
from kivy.uix.image import Image
from kivy.properties import StringProperty, ObjectProperty

from kivymd.uix.behaviors import RoundedRectangularElevationBehavior
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDFlatButton
from kivymd.uix.card import MDCard
from kivymd.uix.dialog import MDDialog
from kivymd.uix.label import MDLabel


class InstanceCard(MDCard, RoundedRectangularElevationBehavior):
    """Implements a material design v3 card."""
    app = App.get_running_app()
    instance = ObjectProperty()

    def __init__(self, **kwargs):
        super(InstanceCard, self).__init__(**kwargs)

        self.ids.image.source = os.path.join(self.app.trial_path, self.instance['properties']['trial'].lower(),
                                             self.instance['properties']['batch'],
                                             'instances', str(self.instance['id'])) + '.cropped.png'

        self. peduncle_path = os.path.join(self.app.trial_path, self.instance['properties']['trial'].lower(),
                                    self.instance['properties']['batch'],
                                    'instances', str(self.instance['id'])) + '.peduncle.png'

        traits = self.instance['traits']

        if self.instance['properties']['orientation'] == 'up':
            self.ids.traits.add_widget(
                MDLabel(
                    text='Peduncle Scar Size: {} mm2'.format(traits['peduncle']['mm2']),
                    adaptive_height=True
                )
            )

            self.ids.images.width = self.ids.images.width + 192
            self.ids.images.add_widget(
                Image(
                    height=168,
                    allow_stretch=True,
                    pos_hint={'top': 0.9375},
                    source=os.path.join(self.app.trial_path, self.instance['properties']['trial'].lower(),
                                        self.instance['properties']['batch'],
                                        'instances', str(self.instance['id'])) + '.peduncle.png'
                )
            )

        self.ids.images.width = self.ids.images.width + 192
        self.ids.images.add_widget(
            Image(
                height=168,
                allow_stretch=True,
                pos_hint={'top': 0.9375},
                source=os.path.join(self.app.trial_path, self.instance['properties']['trial'].lower(),
                                    self.instance['properties']['batch'],
                                    'instances', str(self.instance['id'])) + '.ellipse.png'
            )
        )

        self.ids.traits.add_widget(
            MDLabel(
                text='Hue: {}'.format(round(traits['color']['hue'], 2)),
                adaptive_height=True
            )
        )

        if self.instance['properties']['orientation'] == 'up' or self.instance['properties']['orientation'] == 'down':
            self.ids.traits.add_widget(
                MDLabel(
                    text='Short Axis: {} mm'.format(round(traits['dimensions']['shortAxis'], 2)),
                    adaptive_height=True
                )
            )
            self.ids.traits.add_widget(
                MDLabel(
                    text='Long Axis: {} mm'.format(round(traits['dimensions']['longAxis'], 2)),
                    adaptive_height=True
                )
            )

        if self.instance['properties']['orientation'] == 'side':
            self.ids.traits.add_widget(
                MDLabel(
                    text='Height: {} mm'.format(round(traits['dimensions']['height'], 2)),
                    adaptive_height=True
                )
            )

        self.ids.traits.add_widget(
            MDLabel(
                text='Batch Volume: {} mm3'.format(round(traits['dimensions']['batchVolume'], 2)),
                adaptive_height=True
            )
        )

        self.ids.traits.add_widget(
            MDLabel(
                text='Batch Shape Ratio: {} mm3'.format(round(traits['dimensions']['shapeRatio'], 2)),
                adaptive_height=True
            )
        )