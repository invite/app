# coding:utf-8
import os
import sys
from pathlib import Path

from kivy.config import Config
from kivy.core.window import Window
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.resources import resource_add_path, resource_find

from kivymd.app import MDApp

from libs.vision.segmenter import Segmentron

if getattr(sys, "frozen", False):  # bundle mode with PyInstaller
    os.environ["INVITE_ROOT"] = sys._MEIPASS
else:
    os.environ["INVITE_ROOT"] = str(Path(__file__).parent)
os.environ["INVITE_ASSETS"] = os.path.join(
    os.environ["INVITE_ROOT"], f"assets{os.sep}"
)
Config.set('kivy', 'keyboard_mode', 'systemanddock')
Window.softinput_mode = "below_target"

# Window.maximize()


class InviteApp(MDApp):
    segmenter = Segmentron()

    trial_path = os.path.join('assets', 'trials')
    trial = None
    batch = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.theme_cls.primary_palette = "Teal"

    def build(self):
        return Builder.load_file(os.path.join(
            os.environ["INVITE_ROOT"], "libs", "kv", "home.kv"
        ))

    # TODO: This is for local testing, remove later
    def capture(self):
        camera = self.ids['camera']

    def transition(self, screen, trial=None, batch=None):
        # print(screen, trial, batch)
        manager = self.root.ids.screen_manager
        if trial:
            self.trial = trial
        if batch:
            self.batch = batch

        if not manager.has_screen(screen):
            Builder.load_file(os.path.join(
                os.environ["INVITE_ROOT"], "libs", "kv", f"{screen.lower()}.kv",
            ))

            factory = 'Factory.' + screen + '()'
            screen_object = eval(factory)

            manager.add_widget(screen_object)

        manager.current = screen

    def get_screen(self, screen):
        manager = self.root.ids.screen_manager

        return manager.get_screen(screen)


if __name__ == '__main__':
    os.makedirs(os.path.join('assets', 'trials'), exist_ok=True)

    if hasattr(sys, '_MEIPASS'):
        resource_add_path(os.path.join(sys._MEIPASS))
    InviteApp().run()
