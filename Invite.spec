# -*- mode: python ; coding: utf-8 -*-

from PyInstaller.building.build_main import *
import sys
import os

from kivy_deps import sdl2, glew
from kivymd import hooks_path as kivymd_hooks_path

block_cipher = None


a = Analysis(['main.py'],
             pathex=['/home/guido/Projects/invite-app'],
             binaries=[],
             datas=[("assets\\", "assets\\"), ("libs\\kv\\", "libs\\kv\\"), ("libs\\capturing\\", "libs\\capturing\\"), ("libs\\vision\\", "libs\\vision\\")],
             hiddenimports=[
                "libs.baseclass.batch",
                "libs.baseclass.batch_card",
                "libs.baseclass.card",
                "libs.baseclass.content",
                "libs.baseclass.home",
                "libs.baseclass.instance_card",
                "libs.baseclass.process",
                "libs.baseclass.realsense",
                "libs.baseclass.save",
                "libs.baseclass.toolbar",
                "libs.baseclass.trial",
                "libs.capturing.realsensecapture"
                "libs.vision.peduncle"
                "libs.vision.segmenter"
             ],
             hookspath=[kivymd_hooks_path],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts, 
          [],
          exclude_binaries=True,
          name='Invite',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins)],
               strip=False,
               upx=True,
               upx_exclude=[],
               name='Invite')
