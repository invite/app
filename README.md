# Tomato Trait Detection Application

This repository contains the code for the Tomato Trait Detection Application which is currently under development within the (Horizon2020 Invite Project)[https://www.h2020-invite.eu/]. Initial development foccesses on replicating the setup of examiners at NAK Tuinbouw, who use a Windows tablet for logging DUS scoring activites. The tablet relies on an Intel RealSense D415 camera to generate RGB and 3D images. 


## Workflow
The application allows examiners to create trials under which all traits scorings will be logged. Within a trial the examiner can then capture an image using the connected Intel RealSense D415 camera. The output is then processed using several Computer Vision models using convolutional neural networks. 

1.  Tomato's are segmented and for each individual tomato the orientation is determined. This can be either `UP`, `DOWN`, or `SIDE`. Each tomato is referred to as an `instance`
2.  The orientation then determines which traits can be analysed and which model should be called.
3.  The output of each trait is returned to the batch overview page. Here you can see an image each individually segmented tomatom as well as any relevant traits.

![batch-overview.png](./docs/batch-overview.png)

## Traits
Together with NAK Tuinbouw we have determined a set of traits which are of the most use to them and are suited for initial experiments in terms of complexity. The traits are:
*   Shape Ratio
*   Volume
*   Diameter
*   Peduncle Scar Size

![](./docs/traits-overview.png)


## Output
Each `instance` is stored as an individual image and all extracted traits are visualised on the individual tomato. 

![Segmented instance](./docs/cropped.png)
![Extracted ellipse used for shape ratio and volume](./docs/ellipse.png)
![Segmented peduncle scar size](./docs/peduncle.png)

One large `instances` file is generated which stores all the values related to each `instance`.

###### Example instances.json file
```json
[{
	"id": 1,
	"properties": {
		"orientation": "up",
		"trial": "INVITE Meeting",
		"batch": "1",
		"mmmPerPixel": 0.737
	},
	"traits": {
		"peduncle": {
			"pixels": 57,
			"mm2": 30.96
		},
		"color": {
			"hue": 228.44159113796576
		},
		"dimensions": {
			"shortAxis": 56.8983151473999,
			"longAxis": 57.62149414825439,
			"batchVolume": 83073.34865138074,
			"shapeRatio": 1.126112096416519
		}
	}
}, {
	"id": 2,
	"properties": {
		"orientation": "side",
		"trial": "INVITE Meeting",
		"batch": "1",
		"mmmPerPixel": 0.726
	},
	"traits": {
		"color": {
			"hue": 230.92798690671032
		},
		"dimensions": {
			"height": 46.629730636596676,
			"batchVolume": 83073.34865138074,
			"shapeRatio": 1.126112096416519
		}
	}
}]
```

##### A demonstration of how to use the app
![](./docs/demo.gif)





## Installation and Running the Application

## Requirements
The application is designed to run on both Linux and Windows systems. However, it will not work properly without an Intel RealSense D415 camera. In order to set up your RealSense please refer to the [Realsense SDK](https://www.intelrealsense.com/sdk-2/) installation instructions.

### Installation
```commandline
git clone git@git.wur.nl:invite/app.git

pip install -r requirements.txt
```

## Quickstart

### Run Application
Use the following command to run the application from it's source code

```commandline
python -m main.py
```

### Create Executable
If you want to create an executable file for Windows deployment use the following command

```commandline
python -m PyInstaller Invite.spec
```
